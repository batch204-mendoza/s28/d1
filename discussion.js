//Crud operations
// insert documents (create)
/*

syntax:
insert one document 
db.collectionoName.insertOne({
	"field"A: "valueA",
	"filedB": "valueB"
})

insert many document
db.collectionName.insertMany([
				{
					"fieldA": "valueA",
					"fieldB": "valueB"
				},
				{
					"fieldA": "valueA",
					"fieldB": "valueB"
				}

			])
	

*/

db.users.insertOne(){
	"firstName": "jane",
	"lastName":"doe",
	"age":21,
	"email":"janedoe@mail.com",
	"department": "none"	
}


db.users.insertMany([
	{"firstName": "Stephen",
	"lastName":"Hawkin",
	"age":76,
	"email":"stephenhawking@mail.com",
	"department": "none"	

	},

	{"firstName": "Neil",
	"lastName":"Armstrong",
	"age":,
	"email":"Neilarmstrong@mail.com",
	"department": "none"	

	}

	])

		db.users.insertMany([
		{
			"name": "Javascript 101",
			"price": 5000,
			"description": "Introduction to Javascript",
			"isActive": true
		},	

		{
			"name": "HTML 101",
			"price": 2000,
			"description": "Introduction to HTML",
			"isActive": true
		},
		{
			"name": "CSS 101",
			"price": 2500,
			"description": "Introduction to CSS",
			"isActive": false
		}	

	]
)

// finding documents (read)

/*
snytax:

db.collectionName.find()- this will retrieve all our documents

db.collectionName.find({"criteria":"value"})- rertieves all our documents that will match the criteria

db.colletionName.findoOne({}) find the first thing that document in our collection

db.collectioName.findOne({"critieria:"value"}) will return the first match from document that matches the criteria
*/

db.users.find();

db.users.find({"firstName":"jane"})
db.users.findone({"department":"none"});

//updating documents (update)
/*
syntax:
db.collectionName.updateOne({
	"criteria":"value"
},
{
	$set:{
		"fieldToBeUpdated": "updatedValue"
	

	}

})

*/
db.users.insertOne({
	"firstName": "test",
	"lastName":"Armstrong",
	"age":0,
	"email":"test@mail.com",
	"department": "none"

})


//updatting one documents

db.users.updateOne(
{
	"firstName":"test"
},
{
	$set:{
	"firstName": "Bill",
	"lastName":"gates",
	"age":65,
	"email":"test@mail.com",
	"department": "Operations",
	"status":"active"
}
})

//updating multiple documents
db.users.updateMany({
	"department":"none"

},
{
	$set:{
		"department":"HR"	
	}

}
);


db.users.updateOne({
	"firstName": "jane"
	"lastName" : "doe"
},
	{$set{
		"department": "Operations"
	}
}
)

// removing a field
db.users.updateOne({
	"firstName" : "Bill"

},
{
	$unset: {
		"status" : "active"
	}

}
)

db.users.updateMany({


},
{
	$rename{
		"department": "dept"
	}
}
)